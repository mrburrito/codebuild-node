FROM node:6-alpine

RUN apk add --no-cache python py-pip && \
    pip install awscli
